#ifndef LOG_H
#define LOG_H

#include "api/kit.h"

#include <mutex>

namespace nstd::impl
{

class LogImpl : public kit::Log
{
  public:
    LogImpl();
    ~LogImpl();

    int start(const char* log_path) override;
    int start(kit::LogCallback) override;
    void stop() override;

    template <class... Args> virtual void i(Args...&&) override;
    template <class... Args> virtual void e(Args...&&) override;
    template <class... Args> virtual void w(Args...&&) override;
    template <class... Args> virtual void d(Args...&&) override;
    template <class... Args> virtual void f(Args...&&) override;

  private:
    const char* timestamp();

  private:
    static LogImpl* m_instance;
    static std::mutex m_mutex_instance;

    std::mutex m_mutex_log;
    kit::LogCallback m_callback;
    std::fstream m_log_stream;
    bool m_state;
};
} // namespace nstd::impl

#endif // LOG_H