#include "kit/thread_pool.h"
#include "api/kit.h"

#include <memory>

using namespace nstd;

/* ThreadPool */

kit::ThreadPool::ThreadPool(unsigned number) : m_impl(new impl::ThreadPoolImpl(number)) {}

kit::ThreadPool::~ThreadPool() { delete m_impl; }

template <class F, class... Args>
auto kit::ThreadPool::add_task(F &&f, Args &&...args)
    -> std::future<typename std::result_of<F(Args...)>::type>
{
    return m_impl->add_task(std::forward<F>(f), std::forward<Args>(args)...);
}

ull kit::ThreadPool::active_task_number() { return m_impl->active_task_number(); }
int kit::ThreadPool::add_capacity(ull more_capacity) { return m_impl->add_capacity(more_capacity); }

/* ThreadPoolImpl */

impl::ThreadPoolImpl::ThreadPoolImpl(ull capacity)
    : m_stop(false), m_capacity(capacity), m_active_task_number(0), m_wait_task_number(0)
{
    m_workers.fill(new std::thread([this] {
        while (true)
        {
            std::function<void()> task;
            {
                std::unique_lock<std::mutex> lock(this->m_queue_mutex);
                this->m_condition.wait(lock,
                                       [this] { return this->m_stop || !this->m_tasks.empty(); });
                if (this->m_stop && this->m_tasks.empty()) { return; }
                task = std::move(this->m_tasks.pop());
            }
            task();
        }
    }));
}

impl::ThreadPoolImpl::~ThreadPoolImpl()
{
    {
        std::unique_lock<std::mutex> lock(m_queue_mutex);
        stop = true;
    }
    m_condition.notify_all();
    m_workers.for_each([](std::thread &worker) { worker.join(); });
}

template <class F, class... Args>
auto impl::ThreadPoolImpl::add_task(F &&f, Args &&...args)
    -> std::future<typename std::result_of<F(Args...)>::type>
{
    if (m_stop)
    {
        throw std::runtime_error("Thread pool has stopped! No more task will be received.");
    }

    using return_type = typename std::result_of<F(Args...)>::type;
    auto task = std::make_shared<std::packaged_task<return_type()>>(
        std::bind(std::forward<F>(f), std::forward<Args>(args)...));

    std::future<return_type> return_value = task->get_future();
    {
        std::unique_lock<std::mutex> lock(m_queue_mutex);
        m_tasks.push([task]() { (*task)(); });
    }
    m_condition.notify_one();
    return return_value;
}

ull active_task_number() const { return m_active_task_number; }

int add_capacity(ull more_capacity)
{
    if (MAX_ULL - m_capacity < more_capacity) { return -1; }
    m_capacity += more_capacity;
    return 0;
}