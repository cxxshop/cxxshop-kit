#include "kit/pointer.h"
#include "api/element.h"
#include "api/kit.h"

using namespace nstd;

/* ReferenceCounter */

element::ReferenceCounter::ReferenceCounter() ：m_counter(1) {}
element::ReferenceCounter &element::ReferenceCounter::increase() { ++m_counter; }

element::ReferenceCounter &element::ReferenceCounter::decrease()
{
    if (m_counter > 0) { --m_counter; }
    else { throw std::runtime_error("Out of the lower boundary of unsigned"); }
}

type::ull element::ReferenceCounter::get_number() const { return m_counter.load(); }

/* Pointer */

template <class T> kit::Pointer<T>::Pointer(bool ownership) : m_impl(new PointerImpl<T>(ownership))
{
}

template <class T>
kit::Pointer<T>::Pointer(T *pointer, std::function<void(T *)> deleter)
    : m_impl(new PointerImpl<T>(pointer, deleter))
{
}

template <class T> kit::Pointer<T>::Pointer(const Pointer<T> &other) { m_impl->copy(other.m_impl); }
template <class T> kit::Pointer<T>::~Pointer() { delete m_impl; }
template <class T> T &kit::Pointer<T>::operator*() { return m_impl->dereference(); }
template <class T> T *kit::Pointer<T>::operator->() { return m_impl->get_pointer(); }

template <class T> Pointer<T> &operator=(const Pointer<T> &other)
{
    m_impl->copy(other.m_impl);
    return *this;
}

template <class T> Pointer<T> &operator=(T *pointer)
{
    m_impl->set_pointer(pointer);
    return *this;
}

template <class T, class U> Pointer<T> &kit::Pointer<T>::from_derived(U *pointer)
{
    m_impl->from_derived(pointer);
    return *this;
}

template <class T, class U> Pointer<T> &kit::Pointer<T>::from_base(U *pointer)
{
    m_impl->from_base(pointer);
    return *this;
}

template <class T> bool kit::Pointer<T>::occupied() const { return m_impl->occupied(); }

/* PointerImpl */

template <class T>
impl::PointerImpl<T>::PointerImpl(T *pointer, bool owner, std::function<void(T *)> deleter)
    : m_pointer(pointer), m_owner(owner), m_hold(true), m_deleter(nullptr), m_counter(nullptr)
{
    if (owner)
    {
        m_counter = new element::ReferenceCounter();
        m_deleter = deleter;
    }
}

template <class T> impl::PointerImpl<T>::~PointerImpl() { reset(); }

template <class T> T &impl::PointerImpl<T>::dereference()
{
    if (!m_pointer) { throw std::runtime_error("Attempted to dereference a nullptr"); }
    return *m_pointer;
}

template <class T> T *impl::PointerImpl<T>::get_pointer() { return m_pointer; }

template <class T, class U> void impl::PointerImpl<T>::from_base(U *pointer)
{
    reset();
    m_pointer = std::dynamic_cast<T *>(pointer);
    if (m_pointer)
    {
        m_counter = new element::ReferenceCounter();
        m_hold = true;
    }
}

template <class T, class U> void impl::PointerImpl<T>::from_derived(U *pointer)
{
    reset();
    m_pointer = pointer;
    m_counter = new element::ReferenceCounter();
    m_hold = true;
}

template <class T> void impl::PointerImpl<T>::set_pointer(T *pointer)
{
    reset();
    m_pointer = pointer;
    m_counter = new element::ReferenceCounter();
    m_hold = true;
}

template <class T> void impl::PointerImpl<T>::copy(const PointerImpl *other)
{
    reset();
    m_pointer = other->m_pointer;
    m_counter = other->m_counter;
    m_counter->increase();
}

template <class T> bool impl::PointerImpl<T>::occupied() const { return m_hold; }

template <class T> void impl::PointerImpl<T>::reset()
{
    if (!m_counter) { return; }
    m_counter->decrease();
    if (m_counter->get_number() == 0)
    {
        delete m_counter;
        delete m_pointer;
    }
    m_counter = nullptr;
    m_pointer = nullptr;
    m_deleter = nullptr;
    m_hold = false;
    m_owner = true;
}