#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "api/data.h"
#include "api/kit.h"

#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

namespace nstd::impl
{
class ThreadPoolImpl
{
  public:
    ThreadPoolImpl(nstd::ull capacity);
    ~ThreadPoolImpl();

    template <class F, class... Args>
    auto add_task(F &&, Args &&...) -> std::future<typename std::result_of<F(Args...)>::type>;
    unsigned active_task_number() const;
    int add_capacity(nstd::ull more_capacity);

  private:
    nstd::data::array<std::thread> m_workers;
    nstd::data::queue<std::function<void()>> m_tasks;
    nstd::data::queue<std::function<void()>> m_waiting_tasks;

    nstd::ull m_capacity;
    nstd::ull m_task_number;
    nstd::ull m_active_task_number;

    std::mutex m_queue_mutex;
    std::condition_variable m_condition;
    bool m_stop;
};

} // namespace nstd::impl

#endif
