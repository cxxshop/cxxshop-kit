#include "kit/log.h"

#include <sstream>

impl::LogImpl* impl::LogImpl::m_instance = nullptr;
std::mutex impl::LogImpl::m_mutex_instance;

/* Log */

kit::Log* kit::Log::instance()
{
    std::lock_guard<std::mutex> lock(kit::Log::m_mutex_instance);
    if (!kit::Log::m_instance) { kit::Log::m_instance = new impl::LogImpl(); }
    return kit::Log::m_instance;
}

/* LogImpl */

impl::LogImpl::LogImpl() : m_callback(nullptr), m_state(false) {}

impl::LogImpl::~LogImpl()
{
    if (m_state) { stop(); }
}

int impl::LogImpl::start(const char* log_path)
{
    if (m_state) { stop(); }
    if (!log_path) { return -1; }
    m_log_stream.open(log_path);
    m_state = m_mutex_log.is_open();
    return m_state ? 0 : -1;
}

int impl::LogImpl::start(kit::LogCallback callback)
{
    if (m_state) { stop(); }
    if (!callback) { return -1; }
    m_callback = callback;
    m_state = true;
    return 0;
}

void impl::LogImpl::stop()
{
    if (m_state == false) { return; }
    m_callback = nullptr;
    m_log_path = nullptr;
    m_state = false;
}

const char* impl::LogImpl::timestamp()
{
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    auto time_buffer = std::malloc(32);
    auto p = std::ctime_r(&now, time_buffer);
    *(p + strlen(p) - 1) = '\0';
    return time_buffer;
}

template <class T, class... Args> void impl::LogImpl::i(Args... args)
{
    std::lock_guard<std::mutex> lock(m_mutex_log);
    auto message = m_argument->concate_with(' ', args);

    std::ostringstream oss;
    ((oss << " " << args), ...);
    auto message = oss.str().c_str();

    if (m_callback)
    {
        m_callback(kit::LogLevel::i, message);
        return;
    }

    stream_information << get_timestamp().c_str() << ": [info]: " << message << "\n";
}
