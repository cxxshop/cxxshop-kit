namespace nstd::impl
{
template <class T> class PointerImpl
{
  public:
    PointerImpl(bool ownership);
    PointerImpl(T *pointer, std::function<void(T *)> deleter);
    ~PointerImpl();

    T &dereference();
    T *get_pointer();

    void from_base(U *pointer);
    void from_derived(U *pointer);
    void set_pointer(T *pointer);
    void copy(const PointerImpl *);
    bool occupied() const;

  private:
    void reset();

  private:
    T *m_pointer;
    std::function<void(T *)> m_deleter;
    bool m_occupied;
    bool m_ownership;
    nstd::element::ReferenceCounter *m_counter;
};
} // namespace nstd::impl

// circular reference
// performance
// ownership sematic
